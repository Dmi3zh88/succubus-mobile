export const environment = {
  production: true,
  baseUrl: 'http://ec2-13-53-207-133.eu-north-1.compute.amazonaws.com:8976/api/',
  streamChatWS: 'ws://ec2-13-53-172-13.eu-north-1.compute.amazonaws.com:6135'
};
