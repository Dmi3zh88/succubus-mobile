export enum SliderType {
    EARNINGS = 'earnings',
    TIME = 'time',
    AVG = 'avg'
}

export interface SliderItem {
    type: SliderType;
    value: number | string;
    percents: number;
}
