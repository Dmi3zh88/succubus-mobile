import { Component, Input } from '@angular/core';
import { SliderItem, SliderType } from './slider-type';

@Component({
    selector: 'app-slider-component',
    templateUrl: 'slider.component.html',
    styleUrls: ['slider.component.scss']
})

export class SliderStatisticsComponent {
    @Input() slide: SliderItem;

    constructor() { }

    public get title(): string {
        switch (this.slide.type) {
            case SliderType.EARNINGS:
                return 'Total earnings';

            case SliderType.TIME:
                return 'Total time';

            case SliderType.AVG:
                return 'Avg/hour';

            default:
                return '';
        }
    }

    public get color(): string {
        switch (this.slide.type) {
            case SliderType.EARNINGS:
                return '#F87702';

            case SliderType.TIME:
                return '#3477DB';

            case SliderType.AVG:
                return '#D89B00';

            default:
                return '';
        }
    }

    public get isEarnings(): boolean {
        return this.slide.type === SliderType.EARNINGS;
    }
}
