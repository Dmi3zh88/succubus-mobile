import { Component, OnInit } from '@angular/core';
import { SliderItem, SliderType } from './slider/slider-type';
import { UserService } from '../services/user.service';
import { MainSatistics } from '../models/statistics';
import * as moment from 'moment';


@Component({
    selector: 'app-statistics-page',
    templateUrl: 'statistics.page.html',
    styleUrls: ['statistics.page.scss']
})

export class StatisticsPageComponent implements OnInit {

    public slideOpts = {
        spaceBetween: -60,
        centeredSlides: false,
        pagination: false
    };

    public slides: SliderItem[] = [
        {
            type: SliderType.EARNINGS,
            percents: 32.3,
            value: 1238908.32
        },
        {
            type: SliderType.TIME,
            percents: 23.3,
            value: 320
        },
        {
            type: SliderType.AVG,
            percents: 11.3,
            value: 200
        }
    ];

    public startDate: string;
    public endDate: string;
    public maxDate: string;

    public statistics: MainSatistics;

    constructor(private userService: UserService) { }

    ngOnInit(): void {
        this.maxDate = moment().format('YYYY-MM-DD');
        const period = moment().subtract(1, 'month').format('YYYY-MM-DD');
        this.loadStatistics(period);
    }

    public filter(): void {
        const start = this.startDate ? this.startDate.split('T')[0] : null;
        const end = this.endDate ? this.endDate.split('T')[0] : null;
        this.loadStatistics(start, end);
    }

    private loadStatistics(startDate: string, endDate?: string): void {
        this.userService.getMainStatistics(startDate, endDate).then(res => {
            this.statistics = res;
            this.slides.forEach(item => {
                if (item.type === SliderType.EARNINGS) {
                    item.value = this.statistics.totalEarnings;
                }
                if (item.type === SliderType.TIME) {
                    item.value = this.statistics.totalTime;
                }
                if (item.type === SliderType.AVG) {
                    item.value = this.statistics.avg;
                }
            });
        });
    }

}
