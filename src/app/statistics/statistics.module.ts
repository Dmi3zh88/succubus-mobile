import { NgModule } from '@angular/core';
import { StatisticsPageComponent } from './statistics.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { Flashlight } from '@ionic-native/flashlight/ngx';
import { SliderStatisticsComponent } from './slider/slider.component';
import { GraphsStatisticsComponent } from './graphs/graps.component';
import { FormsModule } from '@angular/forms';


@NgModule({
    declarations: [
        StatisticsPageComponent,
        SliderStatisticsComponent,
        GraphsStatisticsComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: StatisticsPageComponent
            }
        ])
    ],
    providers: [
        Flashlight
    ]
})

export class StatisticsPageModule { }
