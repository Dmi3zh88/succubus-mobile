import { Component, ViewChild, ElementRef, Input, OnChanges } from '@angular/core';
import { MainSatistics } from 'src/app/models/statistics';
import { Chart } from 'chart.js';
import { GraphItems, TableItem } from './graph-items';

@Component({
  selector: 'app-graphs-component',
  templateUrl: 'graphs.component.html',
  styleUrls: ['graphs.component.scss'],
})
export class GraphsStatisticsComponent implements OnChanges {
  @ViewChild('barCanvas') barCanvasRef: ElementRef;
  @ViewChild('pieCanvas') pieCanvasRef: ElementRef;
  public isTabs: boolean;
  @Input() statistics: MainSatistics;
  public tableData: Array<TableItem> = [];

  private tableItems = GraphItems;

  constructor() {}

  ngOnChanges() {
    this.openCircular();
  }

  private loadTableData(): void {
    this.tableData = [];

    const statField = this.isTabs
      ? this.statistics.time
      : this.statistics.earnings;

    for (const key of Object.keys(statField)) {
      const tableItem = this.tableItems.find((item) => item.token === key);
      if (tableItem) {
        this.tableData.push({
          title: tableItem.title,
          color: tableItem.color,
          time: statField[key].time ? statField[key].time : null,
          avg: statField[key].avg ? statField[key].avg : null,
          earnings: statField[key].earnings ? statField[key].earnings : null,
          percent: statField[key].percent ? statField[key].percent : null,
        });
      }
    }
  }

  public openColumnar(): void {
    this.isTabs = true;
    this.loadTableData();
    setTimeout(() => {
      this.drawBarChart();
    }, 0);
  }

  public openCircular(): void {
    this.isTabs = false;
    this.loadTableData();
    setTimeout(() => {
      this.drawPieChart();
    }, 0);
  }

  private drawPieChart(): void {
    const time = this.statistics.time;
    const chart = new Chart(this.pieCanvasRef.nativeElement, {
      type: 'doughnut',
      responsive: true,
      data: {
        datasets: [
          {
            data: [
              time.free_chat && time.free_chat.percent ? time.free_chat.percent : 0,
              time.private_chat && time.private_chat.percent ? time.private_chat.percent : 0,
              time.group_chat && time.group_chat.percent ? time.group_chat.percent : 0,
              time.vip_show && time.vip_show.percent ? time.vip_show.percent : 0,
              time.premium_show && time.premium_show.percent ? time.premium_show.percent : 0,
              time.video_calls && time.video_calls.percent ? time.video_calls.percent : 0,
            ],
            backgroundColor: [
              '#FFB55E',
              '#FD3D86',
              '#4540A4',
              '#56CAF5',
              '#FB625C',
              '#4E73F8',
            ],
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
              },
              display: false,
            },
          ],
          xAxes: [
            {
              display: false,
            },
          ],
        },
        legend: {
          display: false,
        },
      },
    });
  }

  private drawBarChart(): void {
    Chart.defaults.global.datasets.bar.categoryPercentage = 1;
    Chart.defaults.global.datasets.bar.barPercentage = 0.8;
    const time = this.statistics.time;
    const chart = new Chart(this.barCanvasRef.nativeElement, {
      type: 'bar',
      responsive: true,
      data: {
        datasets: [
          {
            label: 'Free chat',
            data: [time && time.free_chat ? time.free_chat.percent : 0],
            backgroundColor: ['#FFB55E'],
          },
          {
            label: 'Private chat',
            data: [time && time.private_chat ? time.private_chat.percent : 0],
            backgroundColor: ['#FD3D86'],
          },
          {
            label: 'Group chat',
            data: [time && time.group_chat ? time.group_chat.percent : 0],
            backgroundColor: ['#4540A4'],
          },
          {
            label: 'Vip show',
            data: [time && time.vip_show ? time.vip_show.percent : 0],
            backgroundColor: ['#56CAF5'],
          },
          {
            label: 'Premium show',
            data: [time && time.premium_show ? time.premium_show.percent : 0],
            backgroundColor: ['#FB625C'],
          },
          {
            label: 'Video calls',
            data: [time && time.video_calls ? time.video_calls.percent : 0],
            backgroundColor: ['#4E73F8'],
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
              },
              display: false,
            },
          ],
          xAxes: [
            {
              display: false,
            },
          ],
        },
        legend: {
          display: false,
        },
      },
    });
  }
}
