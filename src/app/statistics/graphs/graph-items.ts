export const GraphItems: GraphItem[] = [
    {
        token: 'free_chat',
        title: 'Free chat',
        color: '#FFB55E'
    },
    {
        token: 'private_chat',
        title: 'Private chat',
        color: '#FD3D86'
    },
    {
        token: 'group_chat',
        title: 'Group chat',
        color: '#4540A4'
    },
    {
        token: 'video_calls',
        title: 'Video calls',
        color: '#4E73F8'
    },
    {
        token: 'premium_show',
        title: 'Premium show',
        color: '#FB625C'
    },
    {
        token: 'vip_show',
        title: 'Vip show',
        color: '#33CFAC'
    },
    {
        token: 'gift',
        title: 'Gift',
        color: '#997AEE'
    },
    {
        token: 'story',
        title: 'Story',
        color: '#BA34F9'
    },
    {
        token: 'other',
        title: 'Other',
        color: '#B4B8B9'
    }
];

export interface GraphItem {
    token: string;
    title: string;
    color: string;
}

export interface TableItem {
    title: string;
    color: string;
    earnings: number;
    percent: number;
    avg: number;
    time: number;
}
