import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SocialType } from '../interfaces/social';

@Component({
  selector: 'app-socials-component',
  templateUrl: 'socials.component.html',
  styleUrls: ['socials.component.scss'],
})
export class SocialsComponent {
  @Input() activeSocials: SocialType[];
  @Output() socialChange = new EventEmitter<SocialType>();

  public isSocialActive(name: SocialType): boolean {
    return this.activeSocials.includes(name);
  }

  public onSocialChange(name: SocialType): void {
    this.socialChange.next(name);
  }
}
