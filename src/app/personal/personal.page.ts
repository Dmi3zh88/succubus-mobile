import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { PersonalInfo } from '../models/user';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as CONSTANTS from './forms-data';
import { LoaderService } from '../services/loader.service';
import { ToastService } from '../services/toast.service';
import { Observable } from 'rxjs';
import { SocialType } from './interfaces/social';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-personal-page',
  templateUrl: 'personal.page.html',
  styleUrls: ['personal.page.scss'],
})
export class PersonalPageComponent implements OnInit {
  public personalInfo: PersonalInfo;
  public languageNames: Array<string[]> = [];
  public languages: PersonalInfo = {};
  public isAllLanguage: boolean;
  public activeSocials: Observable<SocialType[]>;

  // FORMS
  public personalInformationForm: FormGroup;
  public day: FormControl;
  public month: FormControl;
  public year: FormControl;
  public hairLength: FormControl;
  public body: FormControl;
  public hairColor: FormControl;
  public preference: FormControl;
  public eyeColor: FormControl;
  public breast: FormControl;

  public formData = CONSTANTS;

  constructor(
    private userService: UserService,
    private loaderService: LoaderService,
    private toastService: ToastService
  ) {}

  ngOnInit() {
    this.loaderService.startLoading();
    this.loadSocials();
    this.userService
      .getPersonalInfo()
      .then((res) => {
        this.personalInfo = res;
        this.languageNames = this.formData.LANGUAGES_BASE;
        this.createPersonalInfoFields();
        this.createPersonalInfoForm();
      })
      .catch((_) => this.toastService.present('Something wrong...'))
      .finally(() => this.loaderService.loadingFinished());
  }

  public seeMore(): void {
    this.languageNames.push(this.formData.LANGUAGES_MORE);
    this.isAllLanguage = true;
  }

  private createPersonalInfoFields(): void {
    const birthdate: string[] = this.personalInfo.birthDate.split('-');
    this.day = new FormControl(birthdate[2].split(' ')[0], [
      Validators.required,
      Validators.min(1),
      Validators.max(31),
      Validators.pattern(/^\d+$/gim),
    ]);
    this.month = new FormControl(birthdate[1], [
      Validators.required,
      Validators.min(1),
      Validators.max(12),
      Validators.pattern(/^\d+$/gim),
    ]);
    this.year = new FormControl(birthdate[0], [
      Validators.required,
      Validators.min(1930),
      Validators.max(new Date().getFullYear() - 18),
      Validators.pattern(/^\d+$/gim),
    ]);
    this.hairLength = new FormControl(this.personalInfo.hairLength, [
      Validators.required,
      Validators.min(0),
      Validators.max(200),
      Validators.pattern(/^\d+$/gim),
    ]);
    this.body = new FormControl(this.personalInfo.bodyType, [
      Validators.required,
    ]);
    this.hairColor = new FormControl(this.personalInfo.hairColor, [
      Validators.required,
    ]);
    this.preference = new FormControl(this.personalInfo.sexualPreferences, [
      Validators.required,
    ]);
    this.eyeColor = new FormControl(this.personalInfo.eyeColor, [
      Validators.required,
    ]);
    this.breast = new FormControl(this.personalInfo.breastSize.toString(), [
      Validators.required,
    ]);
  }

  private createPersonalInfoForm(): void {
    this.personalInformationForm = new FormGroup({
      day: this.day,
      month: this.month,
      year: this.year,
      hairLength: this.hairLength,
      hairColor: this.hairColor,
      body: this.body,
      preference: this.preference,
      eyeColor: this.eyeColor,
      breast: this.breast,
    });
  }

  private loadSocials(): void {
    this.activeSocials = this.userService
      .getSocials()
      .pipe(map((items) => items.map((item) => item.name)));
  }

  public saveInformation(): void {
    const newInformation: PersonalInfo = {
      aboutMe: this.personalInfo.aboutMe,
      preferences: this.personalInfo.preferences,
    };
    this.sendChanges(newInformation);
  }

  public changePersonalInformation() {
    this.personalInformationForm.markAllAsTouched();
    if (this.personalInformationForm.valid) {
      const values = this.personalInformationForm.value;
      const changes: PersonalInfo = {
        birthDate: values.year + '-' + values.month + '-' + values.day,
        breastSize: Number(values.breast),
        bodyType: values.body,
        sexualPreferences: values.preference,
        eyeColor: values.eyeColor,
        hairLength: Number(values.hairLength),
        hairColor: values.hairColor,
      };
      this.sendChanges(changes);
    }
  }

  public onBirthdayNotifyChange(): void {
    const changes: PersonalInfo = {
      birthdayNotification: Number(this.personalInfo.birthdayNotification),
    };
    this.sendChanges(changes);
  }

  public onSubscribersNotifyChange(): void {
    const changes: PersonalInfo = {
      streamStartNotification: Number(
        this.personalInfo.streamStartNotification
      ),
    };
    this.sendChanges(changes);
  }

  public onLanguageStateChange(state: boolean, language: string): void {
    this.languages[language] = Number(state);
  }

  public saveLanguageChanges(): void {
    this.sendChanges(this.languages);
  }

  private sendChanges(changes: PersonalInfo): void {
    this.loaderService.startLoading();
    this.userService
      .changePersonalInfo(changes)
      .catch((_) => this.toastService.present('Something wrong...'))
      .finally(() => this.loaderService.loadingFinished());
  }
}
