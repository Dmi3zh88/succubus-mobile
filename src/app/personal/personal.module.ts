import { NgModule } from "@angular/core";
import { PersonalPageComponent } from './personal.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { ToggleItemComponent } from '../components/toggle-item/toggle-item.component';
import { CheckboxModule } from '../components/checkbox/checkbox.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SocialsComponent } from './socials/socials.component';

@NgModule({
    declarations: [
        PersonalPageComponent,
        ToggleItemComponent,
        SocialsComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        CheckboxModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: PersonalPageComponent
            }
        ])
    ]
})

export class PersonalPageModule { }
