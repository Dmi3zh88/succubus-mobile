export const BODY_TYPES = ['Thin', 'Average', 'Curvy'];
export const SEXUAL_PREFERENCES = ['Heterosexual', 'Homosexual', 'Bisexual', 'Solo'];
export const HAIR_COLORS = ['Blond', 'Brunet', 'Redhead', 'Black'];
export const EYE_COLORS = ['Blue', 'Brown', 'Green', 'Gray', 'Lens'];
export const BREAST_SIZES = ['0', '1', '2', '3', '4', '5'];
export const LANGUAGES_BASE = [
    ['spanish', 'german', 'italian'],
    ['french', 'english', 'russian']
];
export const LANGUAGES_MORE = ['turkish', 'arabic'];
