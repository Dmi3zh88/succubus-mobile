export interface Social {
  cost: number;
  description: string;
  link: string;
  name: SocialType;
  status: string;
}

export enum SocialType {
    VK = 'vk',
    INSTAGRAM = 'insta',
    FACEBOOK = 'fb',
    SNAP = 'snap',
    TWITTER = 'tweet'
}
