import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { UserService } from '../services/user.service';
import { UserInfo } from '../models/user';
import { MainSatistics, StatChapter } from '../models/statistics';
import * as moment from 'moment';
import { ToastService } from '../services/toast.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  @ViewChild('barCanvas', { static: false }) barCanvas: ElementRef;
  public userInfo: UserInfo;
  public statistics: MainSatistics;

  constructor(
    private userService: UserService,
    private toastService: ToastService
  ) { }

  ngOnInit(): void {
    this.loadUserInfo();
  }

  public onPeriodChange(event: Event): void {
    const target = event.target as HTMLInputElement;
    const startDate = this.getStartDate(target.value);
    this.loadStatistics(startDate);
  }

  private getStartDate(period: string): string {
    switch (period) {
      case 'day':
        return moment().format('YYYY-MM-DD');

      case 'month':
        return moment().subtract(1, 'month').format('YYYY-MM-DD');

      case 'year':
        return moment().subtract(1, 'year').format('YYYY-MM-DD');

      default:
        return moment().subtract(1, 'week').format('YYYY-MM-DD');
    }
  }

  private drawChars(time: StatChapter): void {
    Chart.defaults.global.datasets.bar.categoryPercentage = 1;
    Chart.defaults.global.datasets.bar.barPercentage = 0.8;
    const chart = new Chart(this.barCanvas.nativeElement, {
      type: 'bar',
      responsive: true,
      data: {
        datasets: [
          {
            label: 'Free chat',
            data: [time && time.free_chat ? time.free_chat.percent : 0],
            backgroundColor: [
              '#FFB55E'
            ]
          },
          {
            label: 'Private chat',
            data: [time && time.private_chat ? time.private_chat.percent : 0],
            backgroundColor: [
              '#FD3D86'
            ]
          },
          {
            label: 'Group chat',
            data: [time && time.group_chat ? time.group_chat.percent : 0],
            backgroundColor: [
              '#4540A4'
            ]
          },
          {
            label: 'Vip show',
            data: [time && time.vip_show ? time.vip_show.percent : 0],
            backgroundColor: [
              '#56CAF5'
            ]
          },
          {
            label: 'Premium show',
            data: [time && time.premium_show ? time.premium_show.percent : 0],
            backgroundColor: [
              '#FB625C'
            ]
          },
          {
            label: 'Video calls',
            data: [time && time.video_calls ? time.video_calls.percent : 0],
            backgroundColor: [
              '#4E73F8'
            ]
          }
        ]
      },
      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true
              },
              display: false
            }
          ],
          xAxes: [
            {
              display: false,
            }
          ]
        },
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            usePointStyle: true,
            fontColor: 'white',
            padding: 20
          }
        },
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        }
      }
    });
  }

  private loadUserInfo(): void {
    this.userService.getUserInfo()
      .then(res => this.userInfo = res)
      .catch(_ => this.toastService.present('Something wrong...'));

    const startDate = this.getStartDate('month');
    this.loadStatistics(startDate);
  }

  private loadStatistics(startDate: string): void {
    this.userService.getMainStatistics(startDate).then(res => {
      this.statistics = res;
      this.drawChars(res.time);
    });
  }

}
