import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-in-online-component',
  templateUrl: 'in-online.page.html',
  styleUrls: ['in-online.page.scss'],
})

export class InOnlineComponent {
  constructor(private router: Router) { }

  public goToStream(): void {
    this.router.navigate(['/stream']);
  }
}
