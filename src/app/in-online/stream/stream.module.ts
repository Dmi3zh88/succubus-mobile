import { NgModule } from '@angular/core';
import { StreamComponent } from './stream.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ParticipantsCounterComponent } from './components/participants-counter/participants-counter.component';
import { StreamSwitchComponent } from './components/stream-switch/stream-switch.component';
import { MessageLineComponent } from './components/message-line/message-line.component';
import { ReportComponent } from './components/report/report.component';
import { SuccessComponent } from './components/success/success.component';
import { CheckboxComponent } from 'src/app/components/checkbox/checkbox.component';
import { ChatMessageComponent } from './components/chat-message/chat-message.component';
import { ModalUsersComponent } from './components/modal-users/modal-users.component';
import { ChatViewerComponent } from './components/chat-viewer/chat-viewer.component';

@NgModule({
  declarations: [
    StreamComponent,
    ParticipantsCounterComponent,
    StreamSwitchComponent,
    MessageLineComponent,
    ReportComponent,
    SuccessComponent,
    CheckboxComponent,
    ChatMessageComponent,
    ModalUsersComponent,
    ChatViewerComponent
  ],
  entryComponents: [ModalUsersComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: StreamComponent,
      },
    ]),
  ],
})
export class StreamPageModule {}
