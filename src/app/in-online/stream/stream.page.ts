import { Component, OnInit, OnDestroy } from '@angular/core';
import { StreamChatService } from 'src/app/services/stream-chat.service';
import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActionType } from './types/messageType';
import { ChatMessage } from './types/chat-message';
import { ModalController } from '@ionic/angular';
import { ModalUsersComponent } from './components/modal-users/modal-users.component';

@Component({
  selector: 'app-stream-component',
  templateUrl: 'stream.page.html',
  styleUrls: ['stream.page.scss'],
})
export class StreamComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject();

  public chatInfoEvent: Observable<any>;
  public messageEvent: Observable<ChatMessage>;
  public messages: ChatMessage[] = [];

  constructor(
    private streamChatService: StreamChatService,
    private modalController: ModalController
  ) {}

  async openModal() {
    const modal = await this.modalController.create({
      component: ModalUsersComponent,
      cssClass: 'global-modal',
      backdropDismiss: false,
    });

    await modal.present();
  }

  ngOnInit() {
    this.streamChatService.connect();
    this.addEventHandlers();

    this.messageEvent.subscribe((res) => this.onMessageReceive(res));
    this.chatInfoEvent = this.streamChatService.chatInfoEvent;

    this.start();
  }

  public onMessageSend(message: string): void {
    this.streamChatService.send(ActionType.MESSAGE, { value: message });
  }

  public onMessageReceive(message: ChatMessage): void {
    this.messages.push(message);
  }

  private start(): void {
    /* (window as any).videoStreamer
      .streamRTMPAuth(
        'rtmp://ec2-13-53-172-13.eu-north-1.compute.amazonaws.com:1935/liveStream',
        console.log('success'),
        console.log('failed')
      )
      .onConnectionSuccess((data) => alert(data))
      .onError((data) => alert(data)); */
    console.log((window as any).videoStreamer);
  }

  private addEventHandlers(): void {
    this.messageEvent = this.streamChatService.messageEvent.pipe(
      takeUntil(this.destroy$)
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
