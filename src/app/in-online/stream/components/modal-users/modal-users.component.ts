import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-users',
  templateUrl: 'modal-users.component.html',
  styleUrls: ['modal-users.component.scss'],
})
export class ModalUsersComponent {

    constructor(private modalController: ModalController,) {}

    closeModal() {
        this.modalController.dismiss();
    }

}