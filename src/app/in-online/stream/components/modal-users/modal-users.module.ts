import { NgModule } from '@angular/core';
import { ModalUsersComponent } from './modal-users.component';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ChatViewerComponent } from '../chat-viewer/chat-viewer.component';

@NgModule({
  declarations: [
    ModalUsersComponent,
    ChatViewerComponent
    ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: ModalUsersComponent,
      }
    ]),
  ],
})
export class ModalUsersModule { }