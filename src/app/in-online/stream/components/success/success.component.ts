import { Component } from '@angular/core';

@Component({
  selector: 'app-success-component',
  templateUrl: 'success.component.html',
  styleUrls: ['success.component.scss'],
})

export class SuccessComponent {}