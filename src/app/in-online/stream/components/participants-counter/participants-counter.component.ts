import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-participants-counter-component',
  templateUrl: 'participants-counter.component.html',
  styleUrls: ['participants-counter.component.scss'],
})

export class ParticipantsCounterComponent {
  @Input() count: number;
}