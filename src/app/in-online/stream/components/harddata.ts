export interface UsersModal {
    userID: string;
    avatar: string;
    name: string;
    statusUser: UserStatus;
    premium: number;
};

interface UserStatus {
    statusCard: CardStatus;
    statusRank: RankStatus;
    statusPoints: number;
}

export enum RankStatus {
    BARON = 'baron',
    DUKE = 'duke',
    KING = 'king',
    COUNT = 'count'
}

export enum CardStatus {
    PRIORITY = 'priority',
    ORANGE = 'orange',
    BLUE = 'blue',
    USUAL = 'usual'
}