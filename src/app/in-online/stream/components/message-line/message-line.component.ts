import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-message-line-component',
  templateUrl: 'message-line.component.html',
  styleUrls: ['message-line.component.scss'],
})

export class MessageLineComponent {
  public message: string;
  @Output() sendMessageEvent = new EventEmitter<string>();

  constructor() {}

  public sendMessage(): void {
    this.sendMessageEvent.next(this.message);
    this.message = '';
  }
}