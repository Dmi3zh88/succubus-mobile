export interface ChatMessage {
  avatar: string;
  date: string;
  from: string;
  isStreamer: boolean;
  userID: string;
  value: string;
}