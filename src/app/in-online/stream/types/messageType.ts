export enum ActionType {
    GET_CONNECT = 'getConnect',
    MESSAGE = 'message',
    MUTE_USER = 'muteUser',
    SYSTEM = 'system',
    CHAT_INFO = 'chatInfo',
    NEW_USER = 'newUser',
    END_USER = 'endUser'
}
