import { NgModule } from '@angular/core';
import { InOnlineComponent } from './in-online.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    InOnlineComponent
    ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: InOnlineComponent,
      }
    ]),
  ],
})
export class InOnlinePageModule { }
