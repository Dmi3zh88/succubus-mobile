import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-avatar-preview-page',
    templateUrl: 'avatar-preview.page.html',
    styleUrls: ['avatar-preview.page.scss']
})

export class AvatarPreviewPage {
    @Input() image: string;

    constructor(
        private modalController: ModalController,
    ) { }

    private closeModal(reload = false): void {
        this.modalController.dismiss({ reload });
    }

    public save(): void {
        this.closeModal();
    }

    public goBack(): void {
        this.closeModal(true);
    }
}
