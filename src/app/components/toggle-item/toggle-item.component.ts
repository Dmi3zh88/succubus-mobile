import { Component, Input } from "@angular/core";

@Component({
    selector: 'app-toggle-item',
    templateUrl: 'toggle-item.component.html',
    styleUrls: ['toggle-item.component.scss']
})

export class ToggleItemComponent {

    @Input() text: string;

    public isOpened = true;

    constructor() { }

    public toggleDropown(): void {
        this.isOpened = !this.isOpened;
    }
 }
