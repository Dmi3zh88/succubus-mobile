import { NgModule } from "@angular/core";
import { ToggleItemComponent } from './toggle-item.component';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        ToggleItemComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ToggleItemComponent
    ]
})

export class ToggleItemModule { }
