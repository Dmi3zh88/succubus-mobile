import { Component, Input, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: 'app-rep-progress',
    templateUrl: 'rep-progress.component.html',
    styleUrls: ['rep-progress.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class RepProgressComponent {

    @Input() value: number;

    constructor() { }

    get progress(): number {
        const valueLength = this.value.toString().length;
        const maxScale = Math.pow(10, valueLength);
        return Math.round((this.value / maxScale) * 100);
    }
}
