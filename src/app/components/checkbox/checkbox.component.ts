import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-checkbox-component',
  templateUrl: 'checkbox.component.html',
  styleUrls: ['checkbox.component.scss'],
})
export class CheckboxComponent {
  @Input() text: string;
  @Input() twotext: string;
  @Input() isChecked: boolean;
  @Output() stateChange = new EventEmitter<boolean>();

  public onChange(): void {
    this.stateChange.next(this.isChecked);
  }
}
