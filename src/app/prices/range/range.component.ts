import { Component, Input, AfterViewInit, Output, EventEmitter } from "@angular/core";
import { RangeStatus } from '../model';

@Component({
    selector: 'app-range',
    templateUrl: 'range.component.html',
    styleUrls: ['range.component.scss']
})

export class RangeComponent implements AfterViewInit {
    @Input() data: RangeStatus;
    @Output() changeValue = new EventEmitter<any>();

    public rangeButtonPosition = 0;

    constructor() { }

    ngAfterViewInit() {
        this.rangeButtonPosition = this.currentValue; 
    }

    public get currentValue(): number {
        const value = (this.data.current / this.data.end) * 100;
        return Math.round(value * 100) / 100;
    }

    public rangeClick({ layerX }, width: number) {
        if (this.data.isOn) {
            this.rangeButtonPosition = Math.round(layerX / width * 100);
            this.data.setted = Math.round(this.rangeButtonPosition * this.data.end) / 100;
        }
    }

    public onTouchMove(event: TouchEvent, width: number) {
        if (this.data.isOn) {
            this.rangeButtonPosition = Math.round(event.targetTouches[0].clientX / width * 100);
            this.data.setted = Math.round(this.rangeButtonPosition * this.data.end) / 100;
        }
    }
}

