import { RangeStatus } from './model';

export const ranges: RangeStatus[] = [
    {
        current: 0,
        end: 0,
        start: 0,
        isOn: true,
        isToggled: false,
        name: 'Private chat',
        token: 'privateChat',
        setted: 0
    },
    {
        current: 0,
        end: 0,
        start: 0,
        isOn: true,
        isToggled: false,
        name: 'Group chat',
        token: 'groupChat',
        setted: 0
    },
    /* {
        current: 0,
        end: 0,
        start: 0,
        isOn: true,
        isToggled: false,
        name: 'Paid chat'
    }, */
    {
        current: 0,
        end: 0,
        start: 0,
        isOn: false,
        isToggled: true,
        name: 'Video call',
        token: 'videoCall',
        setted: 0
    },
    /* {
        current: 0,
        end: 0,
        start: 0,
        isOn: false,
        isToggled: true,
        name: 'Audio call'
    }, */
    {
        current: 0,
        end: 0,
        start: 0,
        isOn: true,
        isToggled: true,
        name: 'Vip show',
        token: 'vipShow',
        setted: 0
    },
    {
        current: 0,
        end: 0,
        start: 0,
        isOn: true,
        isToggled: true,
        name: 'Premium show',
        token: 'premiumShow',
        setted: 0
    },
    {
        current: 0,
        end: 0,
        start: 0,
        isOn: true,
        isToggled: true,
        name: 'Story',
        token: 'story',
        setted: 0
    },
    /* {
        current: 0,
        end: 0,
        start: 0,
        isOn: false,
        isToggled: true,
        name: 'VR show'
    } */
];
