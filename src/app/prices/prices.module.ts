import { NgModule } from "@angular/core";
import { PricesPageComponent } from './prices.page';
import { RangeComponent } from './range/range.component';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        PricesPageComponent,
        RangeComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: PricesPageComponent
            }
        ])
    ]
})

export class PricesPageModule { }
