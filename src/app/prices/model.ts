interface Languages {
    arabic: number;
    english: number;
    french: number;
    german: number;
    italian: number;
    russian: number;
    spanish: number;
    turkish: number;
}

interface Permissions {
    groupChat: boolean;
    personalChat: boolean;
    premiumShow: boolean;
    privateChat: boolean;
    story: boolean;
    videoCall: boolean;
    vipShow: boolean;
}

interface Prices {
    groupChatEnd: number;
    groupChatStart: number;
    personalChatEnd: number;
    personalChatStart: number;
    premiumShowEnd: number;
    premiumShowStart: number;
    privateChatEnd: number;
    privateChatStart: number;
    storyEnd: number;
    storyStart: number;
    videoCallEnd: number;
    videoCallStart: number;
    vipShowEnd: number;
    vipShowStart: number;
}

export interface UserPrice {
    groupChatOn?: number;
    groupChatPrice?: number;
    personalChatOn?: number;
    personalChatPrice?: number;
    premiumShowOn?: number;
    premiumShowPrice?: number;
    privateChatOn?: number;
    privateChatPrice?: number;
    storyOn?: number;
    storyPrice?: number;
    videoCallOn?: number;
    videoCallPrice?: number;
    vipShowOn?: number;
    vipShowPrice?: number;
    streamMenu?: string;
}

export interface PriceInfo {
    languages: Languages;
    permissions: Permissions;
    prices: Prices,
    streamMenu: string;
    user: UserPrice;
}

export interface RangeStatus {
    start: number;
    end: number;
    current: number;
    isOn: boolean;
    name: string;
    isToggled: boolean;
    token: string;
    setted: number;
}
