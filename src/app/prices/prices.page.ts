import { Component, OnInit } from "@angular/core";
import { UserService } from '../services/user.service';
import { ranges } from './ranges';
import { RangeStatus, UserPrice } from './model';
import { ToastService } from '../services/toast.service';
import { LoaderService } from '../services/loader.service';

@Component({
    selector: 'app-prices-page',
    templateUrl: 'prices.page.html',
    styleUrls: ['prices.page.scss']
})

export class PricesPageComponent implements OnInit {

    public ranges: RangeStatus[] = ranges;
    public streamDescription: string;
    public canRender: boolean;

    constructor(
        private userService: UserService,
        private toastService: ToastService,
        private loaderService: LoaderService
    ) { }

    ngOnInit() {
        this.userService.getPrices().then(res => {
            this.ranges.forEach(item => {
                item.start = res.prices[item.token + 'Start'];
                item.end = res.prices[item.token + 'End'];
                item.current = res.user[item.token + 'Price'];
                item.isOn = !!res.user[item.token + 'On'];
                item.setted = res.user[item.token + 'Price'];
            });
            this.streamDescription = res.streamMenu;
            this.canRender = true;
        });
    }

    public savePrices(): void {
        this.loaderService.startLoading();
        let prices: UserPrice = { };
        this.ranges.forEach(item => {
            prices[item.token + 'Price'] = item.setted;
            prices[item.token + 'On'] = Number(item.isOn);
        });
        this.sendData(prices);
    }

    public saveStreamMenuDescription(): void {
        const data = { streamMenu: this.streamDescription };
        this.sendData(data);
    }

    private sendData(data: UserPrice) {
        this.userService.setPrices(data)
        .catch(_ => this.toastService.present('Something wrong...'))
        .finally(() => this.loaderService.loadingFinished());
    }
}
