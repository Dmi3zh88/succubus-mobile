import { CallData, CallStatusType } from './call-item.model';

export const CALLS: CallData[] = [
    {
        id: '0',
        duration: 1832,
        name: 'Anton Ant',
        status: CallStatusType.RECEIVED,
        datetime: '14:00',
        avatar: 'assets/img/user-3.png'
    },
    {
        id: '1',
        duration: 945,
        name: 'Anton Ant',
        status: CallStatusType.CANCELED,
        datetime: '15:00',
        avatar: 'assets/img/user-3.png'
    },
    {
        id: '2',
        duration: 1012,
        name: 'Anton Ant',
        status: CallStatusType.SKIPPED,
        datetime: '16:00',
        avatar: 'assets/img/user-3.png'
    }
];
