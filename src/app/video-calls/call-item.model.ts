export interface CallData {
    id: string;
    name: string;
    status: CallStatusType;
    duration?: number;
    datetime: string;
    avatar: string;
}

export enum CallStatusType {
    RECEIVED = 'received',
    CANCELED = 'canceled',
    SKIPPED = 'skipped'
}

export enum CallType {
    VIDEO_CALL = 'video_call',
    AUDIO_CALL = 'audio_call'
}
