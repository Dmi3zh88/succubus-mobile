import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CallData, CallStatusType } from '../call-item.model';

@Component({
    selector: 'app-call-item',
    templateUrl: 'call-item.component.html',
    styleUrls: ['call-item.component.scss']
})

export class CallItemComponent {
    @Input() data: CallData;
    @Output() doCall = new EventEmitter<CallData>();

    public get isCanceled(): boolean {
        return this.data.status === CallStatusType.CANCELED;
    }

    public get isSkipped(): boolean {
        return this.data.status === CallStatusType.SKIPPED;
    }

    public get isReceived(): boolean {
        return this.data.status === CallStatusType.RECEIVED;
    }

    public selectItem(): void {
        this.doCall.next(this.data);
    }
}
