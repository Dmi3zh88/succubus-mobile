import { Component } from '@angular/core';
import { CALLS } from './calls-list';
import { CallData, CallType } from './call-item.model';

@Component({
  selector: 'app-video-calls-component',
  templateUrl: 'video-calls.page.html',
  styleUrls: ['video-calls.page.scss'],
})
export class VideoCallsComponent {
  public calls = CALLS;
  public isCallPrepare: boolean;
  public callType: CallType;

  public onCallStarted(data: CallData): void {
    this.isCallPrepare = true;
  }

  public onCallEnded(): void {
    this.isCallPrepare = false;
  }

  public onCallTypeSelected(type: CallType): void {
    this.callType = type;
  }
}
