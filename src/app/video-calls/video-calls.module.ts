import { NgModule } from '@angular/core';
import { VideoCallsComponent } from './video-calls.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { CallItemComponent } from './call-item/call-item.component';
import { DurationPipe } from './duration.pipe';
import { CallSelectorComponent } from './call-selector/call-selector.component';
import { DialComponent } from './dial/dial.component';

@NgModule({
  declarations: [
      VideoCallsComponent,
      CallItemComponent,
      DurationPipe,
      CallSelectorComponent,
      DialComponent
    ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: VideoCallsComponent,
      },
    ]),
  ],
})
export class VideoCallsPageModule {}
