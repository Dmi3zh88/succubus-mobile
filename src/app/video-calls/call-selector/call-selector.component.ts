import { Component, Output, EventEmitter } from '@angular/core';
import { CallType } from '../call-item.model';

@Component({
  selector: 'app-call-selector',
  templateUrl: 'call-selector.component.html',
  styleUrls: ['call-selector.component.scss'],
})
export class CallSelectorComponent {
  @Output() selectCall = new EventEmitter<CallType>();

  public selectAudio(): void {
      this.selectCall.next(CallType.AUDIO_CALL);
  }

  public selectVideo(): void {
      this.selectCall.next(CallType.VIDEO_CALL);
  }
}
