import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'appDurationPipe',
})
export class DurationPipe implements PipeTransform {
  transform(value: number): string {
    return moment('1970-01-01 00:00:00')
      .add(value, 'seconds')
      .format('HH:mm:ss');
  }
}
