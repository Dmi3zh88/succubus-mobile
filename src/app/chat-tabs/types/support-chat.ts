export interface SupportMessage {
    chatID: number;
    userAvatar: string;
    userNickName: string;
}
