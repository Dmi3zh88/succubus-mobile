export enum TabType {
    CHAT = 'chat',
    FAVORITES = 'favorites',
    SUPPORT = 'support',
    NOTIFICATIONS = 'notifications',
    TRASH = 'trash'
}
