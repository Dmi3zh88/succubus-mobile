import { Component } from '@angular/core';
import { TabType } from './constants/tab-type';

@Component({
    selector: 'app-chat-tabs-component',
    templateUrl: 'chat-tabs.page.html',
    styleUrls: ['chat-tabs.page.scss']
})

export class ChatTabsComponent {

    public activeTab = TabType.CHAT;

    public selectTab(name: TabType): void {
        this.activeTab = name;
    }
}
