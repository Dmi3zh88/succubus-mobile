import { NgModule } from '@angular/core';
import { ChatTabsComponent } from './chat-tabs.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { MainChatComponent } from './bookmarks/main-chat/main-chat.component';
import { SupportChatComponent } from './bookmarks/support-chat/support-chat.component';

@NgModule({
  declarations: [ChatTabsComponent, MainChatComponent, SupportChatComponent],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChatTabsComponent,
      },
    ]),
  ],
})
export class ChatTabsPageModule {}
