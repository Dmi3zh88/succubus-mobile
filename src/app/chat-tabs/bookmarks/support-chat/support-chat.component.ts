import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { SupportMessage } from '../../types/support-chat';

@Component({
    selector: 'app-support-chat',
    templateUrl: 'support-chat.component.html',
    styleUrls: ['support-chat.component.scss']
})

export class SupportChatComponent implements OnInit {

    public messages: SupportMessage[] = [];

    constructor(private chatService: ChatService) { }

    ngOnInit() {
        this.chatService.getSupportChat().then(res => {
            this.messages = res;
            if (res.length === 0) {
                this.chatService.createSupportChat().then(data => {
                    this.messages = data;
                });
            }
        });
    }
}
