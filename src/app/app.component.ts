import { Platform, ModalController, MenuController } from '@ionic/angular';
import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AvatarPreviewPage } from './components/avatar-preview/avatar-preview.page';
import { MenuItem, menuItems } from './menu';
import { AuthService } from './services/auth.service';
import { ToastService } from './services/toast.service';
import { UserInfo } from './models/user';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  private readonly pageWithMenu = ['home', 'personal', 'income', 'statistic', 'settings', 'chat', 'prices'];
  private readonly imageExtensions = [, '.png', '.jpg', '.jpeg'];
  public avatarBuffer: string | ArrayBuffer;
  public menu: MenuItem[] = menuItems;
  public userInfo: UserInfo;

  @ViewChild('inputFile', { static: false }) inputFileElem: ElementRef;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private modalController: ModalController,
    private menuController: MenuController,
    private authService: AuthService,
    private toastService: ToastService,
    private userService: UserService
  ) {
    this.initializeApp();
  }

  ngOnInit() {
    if (this.authService.isLogin) {
      this.loadUserInfo();
    }
    this.authService.loginStateChange.subscribe(isLogin => {
      if (isLogin) {
        this.loadUserInfo();
      }
    });
  }

  private initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  public get isShowMenu(): boolean {
    const url = this.router.url;
    return this.pageWithMenu.some(path => url.includes(path));
  }

  public loadImage({ target }): void {
    const files = target.files;
    const fileExtension: string = files[0].name.slice(files[0].name.length - 4);
    const isImage = this.imageExtensions.some(ext => fileExtension.includes(ext));
    if (files && files[0] && isImage) {
      const reader = new FileReader();
      reader.onload = (event) => {
        this.avatarBuffer = event.target.result;
        this.openAvatarPreview(this.avatarBuffer);
      };
      const file = target.files[0];
      reader.readAsDataURL(file);
    }
  }

  private async openAvatarPreview(image: string | ArrayBuffer) {
    const modal = await this.modalController.create({
      component: AvatarPreviewPage,
      componentProps: { image }
    });
    modal.onDidDismiss().then(res => {
      if (res.data && res.data.reload) {
        this.openFileManager();
      } else {
        this.userInfo.avatar = this.avatarBuffer.toString();
        const input = this.inputFileElem.nativeElement as HTMLInputElement;
        const blobFile = input.files[0];
        const fd = new FormData();
        fd.append('image', blobFile);
        this.userService.changeAvatar(fd)
          .then(_ => console.log('complete'))
          .catch(_ => this.toastService.present('Something wrong...'));
      }
    });
    modal.present();
  }

  private openFileManager(): void {
    const input = this.inputFileElem.nativeElement as HTMLInputElement;
    input.value = '';
    input.click();
  }

  public closeMenu(): void {
    this.menuController.close();
  }

  public signout(): void {
    if (this.authService.token) {
      this.authService.signout()
        .then(_ => this.authService.clearToken())
        .catch(_ => this.toastService.present('Something wrong...'));
    }
  }

  public loadUserInfo(): void {
    this.userService.getUserInfo()
      .then(res => this.userInfo = res)
      .catch(_ => this.toastService.present('Something wrong...'));
    // .finally
  }
}
