export interface UserInfo {
  accountStatus: string;
  accountType: string;
  avatar: string;
  gender: string;
  id: string;
  interfaceTheme: number;
  kingOfSuccubas: KingField;
  nickName: string;
  nickNameSlug: string;
  persons: number;
  succoins: number;
  verified: boolean;
}

interface KingField {
  rankPoints: number;
  rankStatus: string;
}

export interface PersonalInfo {
  aboutMe?: string;
  preferences?: string;
  birthDate?: string;
  hairLength?: number;
  bodyType?: string;
  sexualPreferences?: string;
  hairColor?: string;
  eyeColor?: string;
  breastSize?: number;
  birthdayNotification?: number;
  streamStartNotification?: number;
  spanish?: number;
  german?: number;
  italian?: number;
  french?: number;
  english?: number;
  russian?: number;
  turkish?: number;
  arabic?: number;
  welcomeMessageOnline?: string;
  welcomeMessageOffline?: string;
}

export interface AccountInfo {
  birthDate: string;
  firstName: string;
  gender: string;
  lastName: string;
  nickName: string;
}
