export interface MainSatistics {
    avg: number;
    earnings: StatChapter;
    time: StatChapter;
    totalEarnings: number;
    totalTime: string;
}

export interface StatChapter {
    free_chat: StatItem;
    gift?: StatItem;
    group_chat: StatItem;
    image?: StatItem;
    other?: StatItem;
    personal_chat?: StatItem;
    premium_show: StatItem;
    private_chat: StatItem;
    story?: StatItem;
    video?: StatItem;
    video_calls: StatItem;
    vip_show: StatItem;
}

interface StatItem {
    earnings?: number;
    percent: number;
    avg?: number;
    time?: string;
}