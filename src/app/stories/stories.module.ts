import { NgModule } from '@angular/core';
import { StoriesPageComponent } from './stories.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { CameraService } from '../services/camera.service';
import { StoryService } from '../services/story.service';
import { UploadGuard } from './services/upload.guard';


@NgModule({
    declarations: [
        StoriesPageComponent,
    ],
    imports: [
        CommonModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: StoriesPageComponent,
                children: [
                    {
                        path: '',
                        redirectTo: 'start-story',
                        pathMatch: 'full'
                    },
                    {
                        path: 'video-story',
                        loadChildren: () => import('./video-story/video-story.module').then(m => m.VideoStoryPageModule),
                        canActivate: [UploadGuard]
                    },
                    {
                        path: 'start-story',
                        loadChildren: () => import('./start-story/start-story.module').then(m => m.StartStoryPageModule)
                    },
                    /* {
                        path: '',
                        loadChildren: () => import('./video-story/video-story.module').then(m => m.VideoStoryPageModule)
                    } */
                ]
            }
        ])
    ],
    providers: [
        CameraService,
        StoryService,
        UploadGuard
    ]
})

export class StoriesPageModule { }
