import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { StoryService } from '../../services/story.service';

@Injectable()

export class UploadGuard implements CanActivate {

    constructor(private storyService: StoryService) { }

    canActivate() {
        return !!this.storyService.preparedFile;
    }
}
