import { Component } from "@angular/core";
import { Flashlight } from '@ionic-native/flashlight/ngx';

@Component({
    selector: 'app-flash-toggle',
    templateUrl: 'flash-toggle.component.html',
    styleUrls: ['flash-toggle.component.scss']
})

export class FlashToggleComponent {

    public isOn: boolean

    constructor(private flashlight: Flashlight) { }

    public toggleFlash(): void {
        this.isOn = !this.isOn;
        this.isOn ? this.flashlight.switchOn() : this.flashlight.switchOff();
    }
}
