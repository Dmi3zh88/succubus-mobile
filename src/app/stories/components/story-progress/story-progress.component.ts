import { Component, Input } from "@angular/core";

@Component({
    selector: 'app-story-progress',
    templateUrl: 'story-progress.component.html',
    styleUrls: ['story-progress.component.scss']
})

export class StoryProgressComponent {
    @Input() stories: Array<{ id: number, progress: number }>;

    constructor() { }


}
