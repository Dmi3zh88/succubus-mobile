export enum StoryFileType {
    IMAGE = 'image',
    VIDEO = 'video'
}

export interface StoryMediaType {
    type: StoryFileType;
    data: string | ArrayBuffer;
    fd: File;
}
