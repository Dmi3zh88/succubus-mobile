import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from '@angular/core';

import { StoryMediaType, StoryFileType } from '../types/story-file-type';
import { StoryService } from 'src/app/services/story.service';

@Component({
  selector: 'app-video-story-page',
  templateUrl: 'video-story.page.html',
  styleUrls: ['video-story.page.scss'],
})
export class VideoStoryPageComponent implements OnInit, OnDestroy {
  @ViewChild('video', { static: true }) videoRef: ElementRef;
  public mediaData: StoryMediaType;
  public StoryFileType = StoryFileType;
  public videoUrl: string | ArrayBuffer;
  public progress = 0;
  public isUploadState: boolean;

  private step = 0;
  private photoInterval: any;
  private readonly maxTime = 15000; // time to show
  private duration: number;
  private video: HTMLVideoElement;

  constructor(private storyService: StoryService) {}

  ngOnInit() {
    this.mediaData = this.storyService.preparedFile;
    if (this.mediaData.type === StoryFileType.VIDEO) {
      this.video = this.videoRef.nativeElement as HTMLVideoElement;
      this.createVideoUrl();
    } else {
      this.createPhotoAction();
    }
  }

  private createVideoUrl() {
    this.videoUrl = this.mediaData.data;
    this.video.onloadedmetadata = () => {
      this.duration = this.video.duration;
    };

    this.photoInterval = setInterval(() => {
      if (this.video.currentTime <= this.duration) {
        this.progress = (this.video.currentTime / this.duration) * 100;
      }
    }, 100);
  }

  private createPhotoAction(): void {
    this.photoInterval = setInterval(() => {
      if (this.step < this.maxTime) {
        this.step += 150;
        this.progress = (this.step / this.maxTime) * 100;
      } else {
        clearInterval(this.photoInterval);
      }
    }, 150);
  }

  public setUpload(): void {
    this.isUploadState = true;
    if (this.storyService.preparedFile.type === StoryFileType.IMAGE) {
      this.storyService.createNewPhotoStory(this.storyService.preparedFile.fd);
    } else {
      // fd.append('video', this.storyService.preparedFile.fd.toString());
      this.storyService.createNewVideoStory(this.storyService.preparedFile.fd);
    }
  }

  ngOnDestroy() {
    clearInterval(this.photoInterval);
  }
}
