import { NgModule } from '@angular/core';
import { VideoStoryPageComponent } from './video-story.page';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        VideoStoryPageComponent,
    ],
    imports: [
    CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: VideoStoryPageComponent
            }
        ])
    ],
    exports: [
        VideoStoryPageComponent
    ]
})

export class VideoStoryPageModule { }
