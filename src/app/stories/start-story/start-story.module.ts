import { NgModule } from '@angular/core';
import { StartStoryPageComponent } from './start-story.page';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlashToggleComponent } from '../components/flash-toggle/flash-toggle.component';
import { Flashlight } from '@ionic-native/flashlight/ngx';

@NgModule({
    declarations: [
        StartStoryPageComponent,
        FlashToggleComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: StartStoryPageComponent
            }
        ])
    ],
    providers: [
        Flashlight
    ]
})

export class StartStoryPageModule { }
