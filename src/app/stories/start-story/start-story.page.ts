import { Component, AfterViewInit } from '@angular/core';
import { CameraService } from 'src/app/services/camera.service';
import { Router } from '@angular/router';
import { StoryFileType } from '../types/story-file-type';
import { ToastService } from 'src/app/services/toast.service';
import { StoryService } from 'src/app/services/story.service';

@Component({
  selector: 'app-start-story-page',
  templateUrl: 'start-story.page.html',
  styleUrls: ['start-story.page.scss'],
})
export class StartStoryPageComponent implements AfterViewInit {
  private readonly maxVideoDuration = 15.5; // seconds

  constructor(
    private cameraService: CameraService,
    private storyService: StoryService,
    private router: Router,
    private toastService: ToastService
  ) {}

  ngAfterViewInit() {
    this.cameraService.startCamera();
    this.storyService.preparedFile = null;
  }

  public switchCamera(): void {
    this.cameraService.switchCamera();
  }

  public loadFile({ target }): void {
    const files = target.files;
    const isImage = files[0].type.includes('image');
    const isVideo = files[0].type.includes('video');
    if (files && files[0] && (isImage || isVideo)) {
      const reader = new FileReader();
      reader.onload = (event) => {
        this.storyService.preparedFile = {
          type: isImage ? StoryFileType.IMAGE : StoryFileType.VIDEO,
          data: event.target.result,
          fd: files[0],
        };
        if (isImage) {
          this.router.navigate(['stories/video-story']);
        } else {
          this.checkVideoDuration(event.target.result).then((isSuccess) => {
            if (isSuccess) {
              this.router.navigate(['stories/video-story']);
            } else {
              this.toastService.present('Max duration of video is 15 seconds');
            }
          });
        }
      };
      const file = target.files[0];
      reader.readAsDataURL(file);
    }
  }

  public takePhoto(): void {
    this.cameraService.takePhoto().then((res) => {
      const image = 'data:image/jpeg;base64,' + res;
      this.storyService.preparedFile = {
        type: StoryFileType.IMAGE,
        data: image,
        fd: this.dataURLtoFile(image, 'photo.jpeg')
      };
      this.router.navigate(['stories/video-story']);
    });
  }

  private dataURLtoFile(dataurl: string, filename: string): File {
    const arr = dataurl.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, { type: mime });
  }

  public onTouchMove(event: TouchEvent): void {
    console.log(event);
  }

  private checkVideoDuration(data: any): Promise<boolean> {
    return new Promise((resolve) => {
      const video = document.createElement('video');
      video.src = data;
      video.onloadedmetadata = () => {
        resolve(video.duration <= this.maxVideoDuration);
      };
    });
  }
}
