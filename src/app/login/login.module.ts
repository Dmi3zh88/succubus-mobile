import { NgModule } from "@angular/core";
import { LoginPageComponent } from './login.page';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CheckboxModule } from '../components/checkbox/checkbox.module';

@NgModule({
    declarations: [
        LoginPageComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        ReactiveFormsModule,
        CheckboxModule,
        RouterModule.forChild([
            {
                path: '',
                component: LoginPageComponent
            }
        ])
    ]
})

export class LoginPageModule {}
