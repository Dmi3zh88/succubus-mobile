import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { SigninData } from '../models/auth';
import { ToastService } from '../services/toast.service';
import { LoaderService } from '../services/loader.service';
import { Router } from '@angular/router';

@Component({
    selector: 'login-page',
    templateUrl: 'login.page.html',
    styleUrls: ['login.page.scss']
})

export class LoginPageComponent implements OnInit {

    public authForm: FormGroup;
    public email: FormControl;
    public password: FormControl;
    private isRemember: boolean;

    constructor(
        private authService: AuthService,
        private toastService: ToastService,
        private loaderService: LoaderService,
        private router: Router
    ) { }

    ngOnInit() {
        this.createFormControls();
        this.createForm();
    }

    private createFormControls(): void {
        this.email = new FormControl('', [Validators.required]);
        this.password = new FormControl('', [Validators.required]);
    }

    private createForm(): void {
        this.authForm = new FormGroup({
            email: this.email,
            password: this.password
        });
    }

    public signin(): void {
        if (this.authForm.valid) {
            this.loaderService.startLoading();
            const userData = this.authForm.value as SigninData;
            this.authService.signin(userData)
                .then(res => {
                    this.authService.setToken(res.token, this.isRemember);
                    this.authService.loginStateChange.next(true);
                    this.router.navigate(['home']);
                })
                .catch(_ => this.toastService.present('Something wrong...'))
                .finally(() => this.loaderService.loadingFinished());
        }
    }

    public changeRememberState(isRemember): void {
        this.isRemember = isRemember;
    }
}
