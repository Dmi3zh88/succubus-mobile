import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { AccountInfo } from '../models/user';
import { LoaderService } from '../services/loader.service';
import { ToastService } from '../services/toast.service';
import { GENDER } from './form-data';
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
} from '@angular/forms';

@Component({
  selector: 'app-settings-page',
  templateUrl: 'settings.page.html',
  styleUrls: ['settings.page.scss'],
})
export class SettingsPageComponent implements OnInit {
  public genders = GENDER;

  public accountInfo: AccountInfo;
  public settingsForm: FormGroup;

  public changePassForm: FormGroup;

  constructor(
    private userService: UserService,
    private loaderService: LoaderService,
    private toastService: ToastService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.settingsForm = this.createForm();
    this.changePassForm = this.createChangePassForm();
    this.userService
      .getAccountInfo()
      .then((res) => {
        this.accountInfo = res;
        this.fillForm();
      })
      .catch((_) => this.toastService.present('Something wrong...'))
      .finally(() => this.loaderService.loadingFinished());
  }

  private createForm(): FormGroup {
    return this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      gender: [''],
      day: [''],
      month: [''],
      year: [''],
    });
  }

  private get validators(): ValidatorFn {
    return Validators.compose([Validators.required]);
  }

  private fillForm(): void {
    const dateSplit = this.accountInfo.birthDate
      ? this.accountInfo.birthDate.split('-')
      : [null, null, null];
    this.settingsForm.patchValue({
      firstName: this.accountInfo.firstName,
      lastName: this.accountInfo.lastName,
      nickName: this.accountInfo.nickName,
      day: dateSplit[2],
      month: dateSplit[1],
      year: dateSplit[0],
      gender: this.accountInfo.gender,
    });
  }

  public createChangePassForm(): FormGroup {
    return this.formBuilder.group({
      email: ['', this.validators],
      password: ['', this.validators],
      newPassword: ['', this.validators],
      newPassword_confirmation: ['', this.validators],
    });
  }

  public savePasswordChanges(): void {
    const isEqual =
      this.changePassForm.value.newPassword ===
      this.changePassForm.value.newPassword_confirmation;
    if (this.changePassForm.valid && isEqual) {
      this.loaderService.startLoading();
      this.userService
        .changePassword(this.changePassForm.value)
        .then(_ => this.changePassForm.reset())
        .catch(({ error }) => {
          const errorText = error && error.error ? error.error : 'Something wrong...';
          this.toastService.present(errorText);
        })
        .finally(() => this.loaderService.loadingFinished());
    }
  }

  public saveUserChanges(): void {
    const formValue = this.settingsForm.value;
    const dateString = `${formValue.year}-${formValue.month}-${formValue.day}`;
    const data = {
      firstName: formValue.firstName,
      lastName: formValue.lastName,
      birthDate: dateString,
      gender: formValue.gender === 'male' ? 1 : 0,
      countryID: 4
    };
    this.loaderService.startLoading();
    this.userService.changeAccountSettings(data)
      .catch(_ => this.toastService.present('Something wrong...'))
      .finally(() => this.loaderService.loadingFinished());
  }
}
