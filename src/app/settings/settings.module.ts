import { NgModule } from "@angular/core";
import { SettingsPageComponent } from './settings.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ToggleItemModule } from '../components/toggle-item/toggle-item.module';

@NgModule({
    declarations: [
        SettingsPageComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        ReactiveFormsModule,
        FormsModule,
        ToggleItemModule,
        RouterModule.forChild([
            {
                path: '',
                component: SettingsPageComponent
            }
        ])
    ]
})

export class SettingsPageModule { }
