import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'personsPipe'
})

export class PersonsPipe implements PipeTransform {
    transform(value: number): string {
        if (value === 1) {
            return '1 person';
        }
        return `${ value } persons`;
    }
}
