import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })


export class DataService {

    private readonly BASE_URL = environment.baseUrl;
    
    constructor(private http: HttpClient) { }

}
