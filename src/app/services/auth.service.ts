import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { SigninData } from '../models/auth';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })


export class AuthService {

    private readonly BASE_URL = environment.baseUrl.concat('auth/');
    private isRemember: boolean;
    private _temporaryToken = '';

    public loginStateChange = new Subject<boolean>();

    constructor(private http: HttpClient) { }

    signin(signinData: SigninData): Promise<{ token: string }> {
        return this.http.post<{ token: string }>(this.BASE_URL.concat('signin'), signinData).toPromise();
    }

    signout(): Promise<any> {
        return this.http.post(this.BASE_URL.concat('signout'), {}).toPromise();
    }

    setToken(value: string, isRemember = false) {
        const token = 'Bearer ' + value;
        this.isRemember = isRemember;
        if (this.isRemember) {
            localStorage.setItem('suc_token', token);
        } else {
            this._temporaryToken = token;
        }
    }

    clearToken(): void {
        this._temporaryToken = null;
        localStorage.removeItem('suc_token');
    }

    get token(): string {
        const token = localStorage.getItem('suc_token') || this._temporaryToken;
        if (token) {
            return token;
        }
        return '';
    }

    get isLogin(): boolean {
        return this.token && this.token.length > 0 ||
            this._temporaryToken && this._temporaryToken.length > 0;
    }
}
