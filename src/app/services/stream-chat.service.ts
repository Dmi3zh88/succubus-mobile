import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { ActionType } from '../in-online/stream/types/messageType';
import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { Subject } from 'rxjs';
import { ChatMessage } from '../in-online/stream/types/chat-message';

@Injectable({ providedIn: 'root' })
export class StreamChatService {

  public chatInfoEvent = new Subject<any>();
  public messageEvent = new Subject<ChatMessage>();

  constructor(
    private socket: Socket,
    private authService: AuthService,
    private userService: UserService
  ) {}

  public connect(): void {
    const token = this.authService.token;
    this.userService.getUserInfo().then(res => {
      this.send(ActionType.GET_CONNECT, { token, room: res.nickName });
    });
    this.socket.on('event', (message) => {
      switch (message.action) {
        case ActionType.CHAT_INFO:
          this.chatInfoEvent.next(message.data);
          break;

        case ActionType.MESSAGE:
          this.messageEvent.next(message.data);
          break;

        default:
          console.log(message);
          break;
      }
    });
  }

  send(action: ActionType, data: any) {
    this.socket.emit('event', { action, data });
  }

}
