import { Injectable } from '@angular/core';

import { ToastController } from '@ionic/angular';

@Injectable({ providedIn: 'root'})

export class ToastService {

    private readonly duration = 5000; // delay in milliseconds for show toast message

    constructor(private toastCtrl: ToastController) { }

    async present(message: string) {
        const toast = await this.toastCtrl.create({
            duration: this.duration,
            message
        });
        toast.present();
    }
}
