import { Injectable } from '@angular/core';
import {
  CameraPreview,
  CameraPreviewPictureOptions,
} from '@ionic-native/camera-preview';

@Injectable()
export class CameraService {
  private readonly options = {
    x: 0,
    y: 0,
    width: window.screen.width,
    height: window.screen.height,
    camera: CameraPreview.CAMERA_DIRECTION.FRONT,
    toBack: true,
    tapPhoto: true,
    tapFocus: false,
    previewDrag: false,
    storeToFile: false,
    disableExifHeaderStripping: false,
  };

  private readonly pictureOpts: CameraPreviewPictureOptions = {
    quality: 100,
  };

  private readonly videoOpts = {
    cameraDirection: CameraPreview.CAMERA_DIRECTION.FRONT,
    quality: 90,
    withFlash: false,
  };

  startCamera(): void {
    setTimeout(() => {
      CameraPreview.startCamera(this.options)
        .then((t) => console.log(t))
        .catch((er) => console.log(er))
        .finally(() => console.log('la final'));
    }, 2000);
  }

  switchCamera(): void {
    CameraPreview.switchCamera();
  }

  takePhoto(): Promise<string> {
    return CameraPreview.takePicture(this.pictureOpts);
  }

  startRecordVideo(): void {
    // this.videoOpts.cameraDirection = cameraDirection;
    CameraPreview.startRecordVideo(this.videoOpts);
  }

  stopRecordVideo(): Promise<string> {
    return CameraPreview.stopRecordVideo();
  }

  stopCamera(): void {
    CameraPreview.stopCamera();
  }
}
