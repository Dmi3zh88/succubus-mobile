import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({ providedIn: 'root'})

export class LoaderService {

    private loaderCounterInQueue = 0;
    private loader: HTMLIonLoadingElement;

    constructor(private loadingCtrl: LoadingController) { }

    async startLoading(count = 1) {
        this.loaderCounterInQueue += count;
        if (!this.loader) {
            this.loader = await this.loadingCtrl.create({
                message: 'Loading'
            });
            this.loader.present();
        }
    }

    async loadingFinished() {
        this.loaderCounterInQueue--;
        if (this.loaderCounterInQueue <= 0 && this.loader) {
            await this.loader.dismiss();
            this.loader = null;
        }
    }
}