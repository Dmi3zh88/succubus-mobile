import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { publishReplay, refCount } from 'rxjs/operators';
import { UserInfo, PersonalInfo, AccountInfo } from '../models/user';
import { MainSatistics } from '../models/statistics';
import { PriceInfo, UserPrice } from '../prices/model';
import { Observable } from 'rxjs';
import { IncomeItem } from '../income/models';
import { Social } from '../personal/interfaces/social';

@Injectable({ providedIn: 'root' })
export class UserService {
  private readonly BASE_URL = environment.baseUrl;
  private userInfoCache: Promise<UserInfo>;

  constructor(private http: HttpClient) {}

  getUserInfo(): Promise<UserInfo> {
    if (!this.userInfoCache) {
      this.userInfoCache = this.http
        .get<UserInfo>(this.BASE_URL.concat('auth/getuserinfo'))
        .pipe(publishReplay(1), refCount())
        .toPromise();
    }
    return this.userInfoCache;
  }

  getMainStatistics(start?: string, end?: string): Promise<MainSatistics> {
    let queryParams: HttpParams;
    if (start && end) {
      queryParams = new HttpParams()
        .set('startDate', start)
        .set('endDate', end);
    }
    if (start && !end) {
      queryParams = new HttpParams().set('startDate', start);
    }
    return this.http
      .get<MainSatistics>(this.BASE_URL.concat('user/statistics?startDate'), {
        params: queryParams,
      })
      .toPromise();
  }

  getPersonalInfo(): Promise<PersonalInfo> {
    return this.http
      .get<PersonalInfo>(this.BASE_URL.concat('user/profile/description'))
      .toPromise();
  }

  changeAvatar(avatar: FormData): Promise<any> {
    return this.http
      .post(this.BASE_URL.concat('user/avatar'), avatar)
      .toPromise();
  }

  changePersonalInfo(data: PersonalInfo): Promise<any> {
    return this.http
      .patch(this.BASE_URL.concat('user/profile/description'), data)
      .toPromise();
  }

  getAccountInfo(): Promise<AccountInfo> {
    return this.http
      .get<AccountInfo>(this.BASE_URL.concat('user/profile/m/accountinfo'))
      .toPromise(); // TODO: temporary url
  }

  getPrices(): Promise<PriceInfo> {
    return this.http
      .get<PriceInfo>(this.BASE_URL.concat('user/profile/price'))
      .toPromise();
  }

  getIncome(start?: string, end?: string): Observable<IncomeItem[]> {
    let queryParams: HttpParams;
    if (start && end) {
      queryParams = new HttpParams()
        .set('startDate', start)
        .set('endDate', end);
    }
    if (start && !end) {
      queryParams = new HttpParams().set('startDate', start);
    }
    return this.http.get<IncomeItem[]>(
      this.BASE_URL.concat('user/statistics/income'),
      { params: queryParams }
    );
  }

  setPrices(data: UserPrice): Promise<any> {
    return this.http
      .patch(this.BASE_URL.concat('user/profile/price'), data)
      .toPromise();
  }

  getSocials(): Observable<Social[]> {
    return this.http.get<Social[]>(this.BASE_URL.concat('user/profile/social'));
  }

  changePassword(data): Promise<any> {
    return this.http
      .post(this.BASE_URL.concat('user/profile/userinfo'), data)
      .toPromise();
  }

  changeAccountSettings(data): Promise<any> {
    return this.http.patch(this.BASE_URL.concat('user/profile/userinfo'), data).toPromise();
  }
}
