import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { StoryMediaType } from '../stories/types/story-file-type';

@Injectable({ providedIn: 'root' })
export class StoryService {
  private readonly BASE_URL = environment.baseUrl;
  public preparedFile: StoryMediaType;

  constructor(private http: HttpClient) {}

  public createNewPhotoStory(image: File): Promise<any> {
    const fd = new FormData();
    fd.append('image1', image);
    fd.append('body', 'My story');
    fd.append('title', 'My story');
    fd.append('type', 'free');
    fd.append('colorTheme', '1');
    return this.http
      .post(this.BASE_URL.concat('user/story/new'), fd)
      .toPromise();
  }

  public createNewVideoStory(video: File): Promise<any> {
    const fd = new FormData();
    fd.append('video', video);
    return this.http
      .post(this.BASE_URL.concat('user/story/newVideo'), fd)
      .toPromise();
  }

  public getLastStory(): Promise<any> {
    return this.http.get(this.BASE_URL.concat('user/story/last')).toPromise();
  }

  public getAllStories(): Promise<any> {
    return this.http.get(this.BASE_URL.concat('user/story/all')).toPromise();
  }

  public deleteStory(storyId: number): Promise<any> {
    return this.http
      .delete(this.BASE_URL.concat('user/story/delete'))
      .toPromise();
  }
}
