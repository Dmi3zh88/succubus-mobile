import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { SupportMessage } from '../chat-tabs/types/support-chat';

@Injectable({ providedIn: 'root' })
export class ChatService {
  private readonly BASE_URL = environment.baseUrl;
  constructor(private http: HttpClient) {}

  public getSupportChat(): Promise<SupportMessage[]> {
    return this.http.get<SupportMessage[]>(this.BASE_URL.concat('user/chat/support')).toPromise();
  }

  public createSupportChat(): Promise<SupportMessage[]> {
    return this.http.get<SupportMessage[]>(this.BASE_URL.concat('user/chat/start?type=support')).toPromise();
  }
}
