import { NgModule } from "@angular/core";
import { ChatPageComponent } from './chat.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { CheckboxModule } from '../components/checkbox/checkbox.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MessageComponent } from './message/message.component';

@NgModule({
    declarations: [
        ChatPageComponent,
        MessageComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        CheckboxModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: ChatPageComponent
            }
        ])
    ]
})

export class ChatPageModule { }