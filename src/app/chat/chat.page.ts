import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MESSAGES } from './messages-data';
import { ChatMessage } from './interfaces/message';

@Component({
  selector: 'app-chat-page',
  templateUrl: 'chat.page.html',
  styleUrls: ['chat.page.scss'],
})
export class ChatPageComponent implements OnInit {
  private rawMessages = MESSAGES;
  public messages: ChatMessage[] = [];
  public isMoreOpen: boolean;
  private readonly currentUserName = 'Angelina Geely';
  @ViewChild('moreMenu') moreMenuRef: ElementRef;

  constructor() {}

  ngOnInit() {
    this.parseMessages();
  }

  private parseMessages(): void {
    let lastMessageVendor = null;
    this.messages = this.rawMessages.reverse().map((message) => {
      if (message.userName === this.currentUserName) {
        message.isSelf = true;
      }
      if (message.userName !== lastMessageVendor) {
        message.isLast = true;
        lastMessageVendor = message.userName;
      }
      return message;
    }).reverse();
  }

  public closeMore(): void {
    this.isMoreOpen = false;
  }

  public toggleMore(): void {
    this.isMoreOpen = !this.isMoreOpen;
    setTimeout(() => {
      this.moreMenuRef.nativeElement.focus();
    }, 0);
  }
}
