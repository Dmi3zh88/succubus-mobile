export interface ChatMessage {
    time: string;
    userName: string;
    isSelf: boolean;
    isLast: boolean;
    avatar: string;
    text: string;
    id: number;
}
