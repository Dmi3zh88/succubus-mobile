import { ChatMessage } from './interfaces/message';

export const MESSAGES: ChatMessage[] = [
    {
        avatar: 'user-2.png',
        id: 0,
        isLast: null,
        isSelf: null,
        time: '12:32',
        userName: 'Angelina Geely',
        text: 'Hello!'
    },
    {
        avatar: 'user-2.png',
        id: 1,
        isLast: null,
        isSelf: null,
        time: '12:33',
        userName: 'Bred Propit',
        text: 'Hello my darling!'
    },
    {
        avatar: 'user-2.png',
        id: 2,
        isLast: null,
        isSelf: null,
        time: '12:40',
        userName: 'Angelina Geely',
        text: 'How are you?'
    },
    {
        avatar: 'user-2.png',
        id: 3,
        isLast: null,
        isSelf: null,
        time: '12:32',
        userName: 'Angelina Geely',
        text: 'How do you do?'
    },
    {
        avatar: 'user-2.png',
        id: 4,
        isLast: null,
        isSelf: null,
        time: '12:32',
        userName: 'Angelina Geely',
        text: 'Hey!!!!'
    },
    {
        avatar: 'user-2.png',
        id: 5,
        isLast: null,
        isSelf: null,
        time: '12:32',
        userName: 'Bred Propit',
        text: 'Im fine'
    },
    {
        avatar: 'user-2.png',
        id: 6,
        isLast: null,
        isSelf: null,
        time: '12:32',
        userName: 'Bred Propit',
        text: 'I told you, Im fine!!'
    }
];
