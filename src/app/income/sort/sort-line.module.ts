import { NgModule } from "@angular/core";
import { SortItemComponent } from './sort-item/sort-item.component';
import { SortLineComponent } from './sort-line.component';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        SortLineComponent,
        SortItemComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        SortLineComponent
    ]
})

export class SortLineModule { }
