import { Component, EventEmitter, Output } from "@angular/core";
import { IncomeSortItem, IncomeSortType, SortWay } from '../models';

@Component({
    selector: 'app-sort-line',
    templateUrl: 'sort-line.component.html',
    styleUrls: ['sort-line.component.scss']
})

export class SortLineComponent {

    @Output() sortChange = new EventEmitter<any>();

    public static activeSort: IncomeSortItem = {
        name: IncomeSortType.TOKENS,
        way: SortWay.ASC
    };

    public sortTypes: IncomeSortItem[] = [
        {
            name: IncomeSortType.PRICE,
            way: SortWay.ASC
        },
        {
            name: IncomeSortType.TOKENS,
            way: SortWay.ASC
        },
        {
            name: IncomeSortType.TIME,
            way: SortWay.ASC
        }
    ];

    constructor() { }

    public onSortChange(): void {
        this.sortChange.next(SortLineComponent.activeSort);
    }
}
