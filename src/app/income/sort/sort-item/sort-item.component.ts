import { Component, Input, Output, EventEmitter } from "@angular/core";
import { IncomeSortItem, SortWay } from '../../models';
import { SortLineComponent } from '../sort-line.component';

@Component({
    selector: 'app-sort-item',
    templateUrl: 'sort-item.component.html',
    styleUrls: ['sort-item.component.scss']
})

export class SortItemComponent {

    @Input() item: IncomeSortItem;
    @Output() activeSortChange = new EventEmitter<any>();

    constructor() { }

    public setActiveSort() {
        let way: SortWay;
        if (SortLineComponent.activeSort.name === this.item.name) {
            way = SortLineComponent.activeSort.way === SortWay.ASC ? SortWay.DESC : SortWay.ASC;
        } else {
            way = SortWay.ASC;
        }
        SortLineComponent.activeSort = { name: this.item.name, way };
        this.activeSortChange.next();
    }

    public get isActive(): boolean {
        return SortLineComponent.activeSort.name === this.item.name;
    }

    public getIsActiveSortWay(way: SortWay): boolean {
        return SortLineComponent.activeSort.way === way;
    }
}
