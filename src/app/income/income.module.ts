import { NgModule } from "@angular/core";
import { IncomePageComponent } from './income.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { IncomePeriodComponent } from './period/period.component';
import { IncomeItemComponent } from './item/income-item.component';
import { SortLineModule } from './sort/sort-line.module';
import { TotalPaidPipe } from './total-paid.pipe';

@NgModule({
    declarations: [
        IncomePageComponent,
        IncomePeriodComponent,
        IncomeItemComponent,
        TotalPaidPipe
    ],
    imports: [
        CommonModule,
        IonicModule,
        SortLineModule,
        RouterModule.forChild([
            {
                path: '',
                component: IncomePageComponent
            }
        ])
    ]
})

export class IncomePageModule { }
