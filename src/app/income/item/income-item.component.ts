import { Component, Input } from "@angular/core";
import { IncomeItem } from '../models';

@Component({
    selector: 'app-income-item',
    templateUrl: 'income-item.component.html',
    styleUrls: ['income-item.component.scss']
})

export class IncomeItemComponent {

    @Input() item: IncomeItem;

    constructor() { }
}
