export enum PeriodType {
    LAST = 'last',
    ALL = 'all'
}

export enum IncomeSortType {
    PRICE = 'price',
    TOKENS = 'tokens',
    TIME = 'time'
}

export enum SortWay {
    ASC = 'ascending',
    DESC = 'descending'
}

export interface IncomeSortItem {
    name: IncomeSortType,
    way: SortWay
}

export interface IncomeItem {
    id: number;
    name: string;
    price: number;
    section: string;
    status: string;
    time: string;
    tokens: string;
}
