import { Component, Output, EventEmitter } from "@angular/core";
import { PeriodType } from '../models';

@Component({
    selector: 'app-income-period',
    templateUrl: 'period.component.html',
    styleUrls: ['period.component.scss']
})

export class IncomePeriodComponent {

    @Output() changePeriod = new EventEmitter<PeriodType>();
    
    constructor() { }

    public onPeriodChange(event: any): void {
        const currentPeriod = event.target.value === PeriodType.ALL ? PeriodType.ALL : PeriodType.LAST;
        this.changePeriod.next(currentPeriod);
    }
}
