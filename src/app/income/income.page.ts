import { Component, OnInit } from "@angular/core";
import { PeriodType, IncomeSortItem, SortWay, IncomeItem } from './models';
import { UserService } from '../services/user.service';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-icome-page',
    templateUrl: 'income.page.html',
    styleUrls: ['income.page.scss']
})

export class IncomePageComponent implements OnInit {

    public incomeData: Observable<any>;
    public totalPaid: number;
    public isShowRange: boolean;
    public maxDate: string;
    private startDate: string;
    private endDate: string;

    constructor(private userService: UserService) { }

    ngOnInit() {
        this.maxDate = moment().format('YYYY-MM-DD');
        this.loadLastDay();
    }

    public onPeriodChange(value: PeriodType): void {
        if (value === PeriodType.LAST) {
            this.loadLastDay();
            this.isShowRange = false;
        } else {
            this.loadFull();
            this.isShowRange = true;
        }
    }

    public onSortChange(sort: IncomeSortItem): void {
        this.incomeData = this.incomeData.pipe(map(items => items.sort((a, b) => {
            if (sort.way === SortWay.ASC) {
                return a[sort.name] - b[sort.name];
            }
            return b[sort.name] - a[sort.name];
         })));
    }

    public applyFilter(): void {
        this.incomeData = this.userService.getIncome(this.startDate, this.endDate);
    }

    public onStartDateChange(event: CustomEvent) {
        this.startDate = event.detail.value.split('T')[0];
    }


    public onEndDateChange(event: CustomEvent) {
        this.endDate = event.detail.value.split('T')[0];
    }

    private loadLastDay(): void {
        const startDate = moment().subtract(1, 'day').format('YYYY-MM-DD');
        this.incomeData = this.userService.getIncome(startDate);
    }

    private loadFull(): void {
        this.incomeData = this.userService.getIncome();
    }

}

