import { Pipe, PipeTransform } from "@angular/core";
import { IncomeItem } from './models';

@Pipe({
    name: 'appTotalPaidPipe'
})

export class TotalPaidPipe implements PipeTransform {
    transform(items: IncomeItem[]): number {
        if (items && items.length > 0) {
            return items.reduce((accum, item) => accum += item.price, 0);
        }
        return 0;
    }
}
