export interface MenuItem {
  id: number;
  name: string;
  path?: string;
  icon: string;
  subitems?: SubItemMenu[];
}

interface SubItemMenu {
  name: string;
  path: string;
}

export const menuItems: MenuItem[] = [
  {
    id: 0,
    name: 'In Online',
    icon: 'online-icon.png',
    path: 'in-online',
  },
  {
    id: 1,
    name: 'Home',
    path: 'home',
    icon: 'home-icon.png',
  },
/*   {
    id: 2,
    name: 'Video calls',
    path: 'video',
    icon: 'video-icon.png',
  }, */
  {
    id: 3,
    name: 'Profile',
    icon: 'profile-icon.png',
    subitems: [
      {
        name: 'Personal information',
        path: 'personal',
      },
      {
        name: 'Prices',
        path: 'prices',
      },
    ],
  },
  {
    id: 4,
    name: 'Statisctics',
    path: 'statistics',
    icon: 'profile-icon.png',
    subitems: [
      {
        name: 'General statistics',
        path: '',
      },
      {
        name: 'Income',
        path: 'income',
      },
    ],
  },
  {
    id: 5,
    name: 'Acount settings',
    path: 'settings',
    icon: 'settings-icon.png',
  },
  {
    id: 6,
    name: 'Chat',
    path: 'chat-tabs',
    icon: 'chat-icon.png',
  },
  {
    id: 7,
    name: 'Log out',
    path: 'login',
    icon: 'logout-icon.png',
  },
];
